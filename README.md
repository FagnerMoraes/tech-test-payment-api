## INSTRUÇÕES PARA RODAR O PROJETO

-CLONAR O REPOSITORIO
-ACESSAR A PASTA = tech-test-payment-api
- EXECUTAR = DOTNET RESTORE
- ACESSAR A PASTA ./SRC/PAYMENTAPI.API
- EXECUTAR = DOTNET RUN 

## DADOS PARA TEST
- CODIGOS PRODUTOS: 1,2
- CODIGOS VENDEDORES: 1,2

## BIBLIOTECAS UTILIZADAS NO PROJETO

- Microsoft.EntityFrameworkCore;
- Microsoft.EntityFrameworkCore.Sqlite;
- Microsoft.EntityFrameworkCore.Design;
- Microsoft.EntityFrameworkCore.Tools.

## BIBLIOTECAS PARA OS TESTES
- XUNIT;
- FluentAssertions;
- NSubstitute;
